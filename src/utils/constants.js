export const ENV = {
    API_URL: "https://evaluacion-2.vercel.app/api/",
    ENDPOINTS: {
        LOGIN: "auth/signin",
        REGISTER: "users",
        USERS: "users"
    },
    STORAGE: {
        TOKEN: "token"
    }
}