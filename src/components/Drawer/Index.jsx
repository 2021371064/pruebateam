import React, { useState } from "react";
import {Drawer, Avatar, Button, Input} from "antd";
import {MailOutlined, UserOutlined} from "@ant-design/icons";
import "./Drawer.css"
import {useAuth} from "../../hooks/useAuth.js";
import EditProfile from "../Modals/EditProfile/EditProfile.jsx";
import {changeUser} from "../../services/changeUser.js";

const DrawerComponent = () => {
    const [open, setOpen] = useState(false);
    // Traer los datos del usuario y cerrar sesión
    const { user, logout } = useAuth()
    // Variables para el cambio de datos
    const [editName, setEditName] = useState("");
    const [editModalVisible, setEditModalVisible] = useState(false);

    const showDrawer = () => {
        setOpen(true);
    };
    const onClose = () => {
        setOpen(false);
    }

    const toggleEditModal = () => {
        setEditModalVisible(!editModalVisible);
        // abrir la modal con el nombre del usuario
        if (!editModalVisible) {
            setEditName(`${user[0].name} ${user[0].lastname}`);
        }
    };

    const handleSaveEditedName = (newName, newLastName) => {
        // Consumur la api para cambiar el nombre del usuario
        setEditModalVisible(false);
        try {
            changeUser(newName, newLastName)
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <>
            <Avatar
                onClick={showDrawer}
                size={44}
                style={{ backgroundColor: "#87d068", cursor: "pointer" }}
                icon={<UserOutlined />}
            />
            <Drawer title="Sistema Escolar" onClose={onClose} open={open}>
                <div className="drawer-content">
                    <div className="drawer-item">
                        <MailOutlined
                            style={{ marginRight: 8, fontSize: 18, color: "#1890ff" }}
                        />
                        <span className="drawer-text">Correo:</span>{" "}
                        <span style={{ color: "#333", fontWeight: "bold" }}>
                            {user[0].email}
                        </span>
                    </div>
                    <div className="drawer-item">
                        <UserOutlined
                            style={{ marginRight: 8, fontSize: 18, color: "#1890ff" }}
                        />
                        <span className="drawer-text">Nombre:</span>{" "}
                        <span style={{ color: "#333", fontWeight: "bold", marginRight: 8}}>
                            {user[0].name.toUpperCase()} {user[0].lastname.toUpperCase()}
                        </span>
                        {" "}
                        <Button type="primary" onClick={toggleEditModal}>
                            Editar
                        </Button>
                    </div>
                    <button onClick={logout} className="btn-drawer">
                        Cerrar Sesión
                    </button>
                </div>
            </Drawer>

            <EditProfile
                visible={editModalVisible}
                initialName={`${user[0].name}`}
                lastName={`${user[0].lastname}`}
                onCancel={toggleEditModal}
                onSave={handleSaveEditedName}
            />

        </>
    )
}
export default DrawerComponent