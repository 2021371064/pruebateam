import {Button, Input, Modal} from "antd";
import {useState} from "react";
import {validateFullName} from "./validations.js";

const EditProfile = ({ visible, initialName, lastName,  onCancel, onSave }) => {
    const [editedName, setEditedName] = useState(initialName);
    const [editedLastName, setEditedLastName] = useState(lastName);
    const [errorsAlert, setErrorsAlert] = useState(false);
    const [errors, setErrors] = useState("");

    const handleSave = () => {
        // Función para las validaciones
        if (!validateFullName(editedName, editedLastName)) {
            // Mostrar mensaje de error si el nombre no es válido
            setErrors("Por favor ingrese un nombre válido sin números ni caracteres especiales, excepto la letra 'ñ' y acentos.");
            setErrorsAlert(true)
            return;
        }

        onSave(editedName, editedLastName);
        setEditedName("");
        setEditedLastName("");
        setErrors("");
        setErrorsAlert(false)
    };

    return (
        <Modal
            className={"edit-name-modal"}
            title={"Editar Nombre"}
            visible={visible}
            onCancel={onCancel}
            footer={[
                <Button key={"cancel"} onClick={onCancel}>
                    Cancelar
                </Button>,
                <Button key={"save"} type={"primary"} onClick={handleSave}>
                    Guardar
                </Button>,
            ]}
        >
            <div style={{ marginBottom: 16 }}>
                {errorsAlert ? (
                    <div style={{ color: "red", marginBottom: 12 }}>
                        {errors}
                    </div>
                ) : null}
                <Input
                    value={editedName}
                    onChange={(e) => setEditedName(e.target.value)}
                    placeholder="Ingrese el nombre(s)"
                    style={{ marginBottom: 12 }}
                />
                <Input
                    value={editedLastName}
                    onChange={(e) => setEditedLastName(e.target.value)}
                    placeholder="Ingrese los apellidos"
                />
            </div>
        </Modal>
    );
}
export default EditProfile